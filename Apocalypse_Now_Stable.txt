<?xml version="1.0" encoding="UTF-8"?>
<Servers>
   <ServerGroup Name="Apocalypse Now" Logo="" Description="">
      <Server Name="Apocalypse Now Stable" Logo="">
         <Description>
Welcome to Apocalypse Now Overhaul Mod For 7 Days To Die

Discord Server :- https://discord.gg/GS3wGQTu6U

Features:--

-Progression Tree Overhaul
-Added Game-Stage Difficulty Bonus
-Added Supply Drops , You can now call Airdrops 
-7 new Classes " Engineer, Survivalist, Hunter, Medic, Farmer, Miner and Scavenger"
-144 slot backpack! 24 slot crafting queue.!
-Custom UI! Food/Water bars.
-Increased zombie spawn/respawn rate.!
-Night time is now Hell. Don't Blame Me if you died.;p
-Titanium Nodes added! Can be found in the wasteland. 
-Wasteland is now Radiated!
-Added The Horent Back
-Disabled the Zombie Rage
-Added Pin Recipes Modlet 
-Added Active Ingredients Modlet
-More crops! Grow Apple, Cherry, Banana, Coconut and Lemon trees, plus strawberries, cabbage, carrot, cucumber and tomatoes.
-Big list of melee weapons have been added , from Swords, Bats, Pitchforks, Maces, Frying Pan, Cleaver and more
-Added New Custom Food/Drinks• 
-Added Higher Tier Weapons And Tools
-Added Scrap Tier Tools•
-Added 27 New item_modifiers
-Legendary Tier weapons have been added
-Gun parts are back
-Added Bdubs Vehciles Modlet
-Added 10 Maps With ZZTong-Prefabs• 
-Added Stainless Steel, Titanium and Reinforced Titanium Blocks •
-Traders are Destructible and open all the time• 
-Added New Ammo and explosives
-Added Zombie Bosses Including the behemoth
-The Candy is Craft-able Now
-8 new workstations , 2 Advanced Forges, 2 Advanced Workbenches 
An Advanced chemistry Station, Advanced Cement Mixer And Upgrade Bench
-Added Research Desk that you can Unlock Schematics and books volume through it

Class Information :-

Engineer Class Training Grants you :- Bicycle Chassis and Handlebars.

Survivalist Class Training Grants you :- Cloth Armor Bundle.

Hunter Class Training Grants you :- Hunting Rifle Bundle.

Medic Class Training Grants you :- 4 First Aid Bandage, 2 First Aid Kit, 2 Painkillers, 2 Herbal Antibiotics and 2 Plaster Cast.

Farmer Class Training Grants you :- Food Bundle 01.

Miner Class Training Grants you :- Scrap Armor Bundle.

Scavenger Class Training Grants you :- (150) 7.62mm Ammo,(150) 9mm Ammo and (150) Shotgun Shell.

	 </Description>
	 <Discord>https://discord.gg/GS3wGQTu6U</Discord>
	 <Version>Latest</Version>
	 <DownloadMode>Clone</DownloadMode>
         <Downloads>
			<Download>https://gitlab.com/killerbunny264/Apoc-Now-V3-1-Stable</Download>
         </Downloads>
      </Server>
	  </ServerGroup>
	    
</Servers>